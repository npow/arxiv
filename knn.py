#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division # force floating point division
import csv
import math
import numpy as np
import re
import sys
import time

from bisect import bisect_left
from random import randrange
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.cross_validation import KFold, StratifiedKFold
from sklearn.decomposition import TruncatedSVD, PCA, RandomizedPCA
from sklearn.externals import joblib
from sklearn.neighbors import BallTree
from sklearn import metrics
from sklearn import preprocessing
from sklearn import random_projection
from sklearn.cross_validation import train_test_split

TRAIN_INPUT_FILE = 'data/train_input_10000.csv'
TRAIN_OUTPUT_FILE = 'data/train_output_10000.csv'
TEST_INPUT_FILE = 'data/test_input_10000.csv'
DUMP_TOKENS = False
DO_CROSSVALIDATION = True

abstract_data = list(csv.DictReader(open(TRAIN_INPUT_FILE), delimiter=',', quotechar='"'))
category_data = list(csv.DictReader(open(TRAIN_OUTPUT_FILE), delimiter=',', quotechar='"'))
abstracts = np.array([item['abstract'] for item in abstract_data])
categories = [item['category'] for item in category_data]

uniq_categories = list(set(categories))
le = preprocessing.LabelEncoder()
le.fit(uniq_categories)
Y = le.transform(categories)

def get_stop_words():
  f = open('data/stopwords.txt')
  L = []
  for w in f:
    L.append(w.strip())
  return set(L)

stops = get_stop_words()

_digits = re.compile('\d')
_weird_chars = re.compile('( |\/|\.|,|;|:|\(|\)|"|\?|!|®|ᴹᴰ|™|\*|\{|\}|\$|_|\\\\|`|\'|\]|\[|<|\+|=|\^|\-|~)')
def contains_digits(d):
  return bool(_digits.search(d))

def tokenize(s):
  s = s.lower()
  tokens = _weird_chars.split(s)
  tokens = filter(lambda x: len(x) > 2 and x not in stops and not contains_digits(x), tokens)
  return tokens

### Locality Sensitive Hashing ###
### see http://infolab.stanford.edu/~ullman/mmds/ch3a.pdf
class LSH_KNN:
  def __init__(self, k):
    self.k = k
    self.nbits = 1000
    self.H = {}
    self.HH = {}
    self.randv = None

  def hash(self, x):
    h = 0
    for p in self.randv:
      h = h << 1
      v = np.dot(p, x.T.todense())
      if v >= 0:
        h |= 1
    return h

  def fit(self, X, Y):
    self.randv = np.random.randn(self.nbits, X.shape[1])
    self.X = X
    self.Y = Y
    self.Nc = np.unique(Y).shape[0]
    for i, x in enumerate(X):
      h = self.hash(x)
      if not h in self.H:
        self.H[h] = []
      self.H[h].append(Y[i])
    for h in self.H:
      neighbors = self.H[h]
      cnt = np.zeros((self.Nc, 1))
      for c in neighbors:
        cnt[c] += 1
      self.HH[h] = np.argmax(cnt)

  def predict1(self, x):
    h = self.hash(x)
    if not h in self.HH:
      return le.transform(['math'])[0] # no neighbors ?!
    return self.HH[h]

  def predict(self, X):
    pred = np.zeros((X.shape[0], 1))
    for i, x in enumerate(X):
      pred[i][0] = self.predict1(x)
    return pred

### THIS IS DOG SLOW!!! ###
class KNN:
  def __init__(self, k):
    self.k = k

  def fit(self, X, Y):
    self.X = X
    self.Y = Y
    self.ballTree = BallTree(X, leaf_size=2) 
    self.Nc = np.unique(Y).shape[0]

  def predict1(self, x):
    dist, ind = self.ballTree.query(x, k=self.k)
    cnt = np.zeros((self.Nc, 1))
    for i in ind:
      c = self.Y[i]
      cnt[c] += 1

    return np.argmax(cnt)

  def predict(self, X):
    pred = np.zeros((X.shape[0], 1))
    for i, x in enumerate(X):
      pred[i][0] = self.predict1(x)
    return pred

if DUMP_TOKENS:
  wc = {}
  for abstract in abstracts:
    tokens = tokenize(abstract)
    for token in tokens:
      if not token in wc:
        wc[token] = 0
      wc[token] += 1

  top_tokens = sorted(wc, key=wc.get, reverse=True)
  f = open('sorted_tokens.txt', 'wb')
  for token in top_tokens:
    f.write("%s\n" % token)
  f.close()

def get_top_tokens():
  f = open('sorted_tokens.txt')
  tokens = []
  for token in f:
    tokens.append(token.strip())
  return tokens
top_tokens = get_top_tokens()
print "HERE"

if DO_CROSSVALIDATION:
  skf = StratifiedKFold(Y, n_folds=4, shuffle=True, random_state=42)
  X = abstracts
  for train_indices, test_indices in skf:
    X_train = X[train_indices]
    Y_train = Y[train_indices]
    X_test = X[test_indices]
    Y_test = Y[test_indices]
    vectorizer = TfidfVectorizer(tokenizer=tokenize, ngram_range=(1,1))
    X_train = vectorizer.fit_transform(X_train)
    X_test = vectorizer.transform(X_test)
    select = TruncatedSVD(n_components=26)
    X_train = select.fit_transform(X_train)
    X_test = select.transform(X_test)

    clf = KNN(2)
    clf.fit(X_train, Y_train)
    pred = clf.predict(X_test)
    score = metrics.f1_score(Y_test, pred)
    print metrics.classification_report(Y_test, pred)
    print metrics.confusion_matrix(Y_test, pred) 
else:
  abstracts_train, abstracts_test, Y_train, Y_test = train_test_split(abstracts, Y, test_size=0.2, random_state=42)
  for num_tokens in range(1, 100):
    print "*" * 80
    print "num_tokens: %d" % num_tokens
    vocabulary = top_tokens#[:num_tokens]
    vectorizer = TfidfVectorizer(tokenizer=tokenize, ngram_range=(1,1), vocabulary=vocabulary, use_bm25idf=True, sublinear_tf=True)
    X_train = vectorizer.fit_transform(abstracts_train)
    X_test = vectorizer.transform(abstracts_test)
    select = TruncatedSVD(n_components=num_tokens)
    X_train = select.fit_transform(X_train)
    X_test = select.transform(X_test)
    for k in xrange(1, 5):
      #clf = LSH_KNN(k)
      clf = KNN(k)
      
      print "k: %d" % k
      clf.fit(X_train, Y_train)
      pred = clf.predict(X_test)
      score = metrics.f1_score(Y_test, pred)
      print metrics.classification_report(Y_test, pred)
      print metrics.confusion_matrix(Y_test, pred) 
      sys.stdout.flush()
