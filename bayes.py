#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division # force floating point division
import csv
import math
import numpy as np
import re
import time

from random import randrange
from sklearn.cross_validation import KFold, StratifiedKFold
from sklearn.externals import joblib
from sklearn import preprocessing
from sklearn import metrics
from sklearn.cross_validation import train_test_split

TRAIN_INPUT_FILE = 'data/train_input.csv'
TRAIN_OUTPUT_FILE = 'data/train_output.csv'
TEST_INPUT_FILE = 'data/test_input.csv'

LIMIT_FEATURES = False

abstract_data = list(csv.DictReader(open(TRAIN_INPUT_FILE), delimiter=',', quotechar='"'))
category_data = list(csv.DictReader(open(TRAIN_OUTPUT_FILE), delimiter=',', quotechar='"'))
abstracts = np.array([item['abstract'] for item in abstract_data])
categories = [item['category'] for item in category_data]

uniq_categories = list(set(categories))
le = preprocessing.LabelEncoder()
le.fit(uniq_categories)
Y = le.transform(categories)

def get_stop_words():
  f = open('data/stopwords.txt')
  L = []
  for w in f:
    L.append(w.strip())
  return set(L)

stops = get_stop_words()

_digits = re.compile('\d')
_weird_chars = re.compile('( |\/|\.|,|;|:|\(|\)|"|\?|!|®|ᴹᴰ|™|\*|\{|\}|\$|_|\\\\|`|\'|\]|\[|<|\+|=|\^|\-|~)')
def contains_digits(d):
  return bool(_digits.search(d))

def tokenize(s):
  s = s.lower()
  tokens = _weird_chars.split(s)
  tokens = filter(lambda x: len(x) > 2 and x not in stops and not contains_digits(x), tokens)
  return tokens

class MultiNaiveBayes:
  def __init__(self):
    self.N = 0
    self.Nc = {}
    self.prior = {}
    self.vocabulary = set()
    self.Tct = {}
    self.condprob = {}
    self.sum_Tct = {}
    self.alpha = 1
    self.df = {}
    self.top_words = set()
    self.num_features = None

  def fit(self, X, Y):
    self.N = len(Y)
    self.df = {}
    for x in X:
      words = tokenize(x)
      for word in words:
        self.vocabulary.add(word)
        if not word in self.df:
          self.df[word] = 0
        self.df[word] += 1
    if self.num_features is not None:
      self.top_words = set(sorted(self.df, key=self.df.get, reverse=True)[:self.num_features])
    else:
      self.top_words = self.vocabulary
    for c in np.unique(Y):
      indices = np.where(Y == c)
      self.Nc[c] = len(indices) 
      self.prior[c] = self.Nc[c] / self.N
      self.Tct[c] = {}
      self.condprob[c] = {}
      for t in self.top_words:
        self.Tct[c][t] = 0
      for x in X[indices]:
        words = tokenize(x)
        for word in words:
          if not word in self.Tct[c]:
            self.Tct[c][word] = 0
          self.Tct[c][word] += 1

    for c in self.Nc:
      self.sum_Tct[c] = sum([self.Tct[c][t]+self.alpha for t in self.Tct[c]])
      for t in self.top_words:
        self.condprob[c][t] = (self.Tct[c][t] + self.alpha) / self.sum_Tct[c]

  def set_num_features(self, num_features):
    self.num_features = num_features

  def predict_proba(self, X, category):
    if category == None:
      return [self.predict_proba(X, c) for c in self.Nc]
    score = math.log(self.prior[category])
    for x in X:
      words = tokenize(x)
      for word in words:
        if word in self.top_words:
          score += math.log(self.condprob[category][word])
    return score

  def predict(self, X):
    pred = np.zeros((X.shape[0], 1))
    for i, x in enumerate(X):
      ps = self.predict_proba(tokenize(x), None)
      pred[i][0] = np.argmax(ps)
    return pred

if LIMIT_FEATURES:
  X_train, X_test, Y_train, Y_test = train_test_split(abstracts, Y, test_size=0.2, random_state=42)
  clf = MultiNaiveBayes()
  for k in xrange(500, 100000, 1000):
    print "max_features: %d" % k
    clf.set_num_features(k)
    print "begin fit"
    clf.fit(X_train, Y_train)
    print "done fit"
    pred = clf.predict(X_test)
    score = metrics.f1_score(Y_test, pred)
    print metrics.classification_report(Y_test, pred)
    print metrics.confusion_matrix(Y_test, pred) 
else:
  X = abstracts
  skf = StratifiedKFold(Y, n_folds=4, shuffle=True, random_state=42)
  for train_indices, test_indices in skf:
    X_train = X[train_indices]
    Y_train = Y[train_indices]
    X_test = X[test_indices]
    Y_test = Y[test_indices]

    clf = MultiNaiveBayes()
    print "begin fit"
    clf.fit(X_train, Y_train)
    print "done fit"
    pred = clf.predict(X_test)
    score = metrics.f1_score(Y_test, pred)
    print metrics.classification_report(Y_test, pred)
    print metrics.confusion_matrix(Y_test, pred) 
