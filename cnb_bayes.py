#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division # force floating point division
import csv
import math
import numpy as np
import re
import time

from random import randrange
from sklearn.cross_validation import KFold
from sklearn.externals import joblib
from sklearn import preprocessing
from sklearn import metrics
from sklearn.cross_validation import train_test_split

TRAIN_INPUT_FILE = 'data/train_input_10000.csv'
TRAIN_OUTPUT_FILE = 'data/train_output_10000.csv'
TEST_INPUT_FILE = 'data/test_input_10000.csv'

USE_COMPLEMENT = False
NORMALIZE_WEIGHTS = False

USE_IDF = False
SUBLINEAR_TF = False
NORMALIZE_DIST = False

abstract_data = list(csv.DictReader(open(TRAIN_INPUT_FILE), delimiter=',', quotechar='"'))
category_data = list(csv.DictReader(open(TRAIN_OUTPUT_FILE), delimiter=',', quotechar='"'))
titles = np.array([item['abstract'] for item in abstract_data])
categories = [item['category'] for item in category_data]

uniq_categories = list(set(categories))
le = preprocessing.LabelEncoder()
le.fit(uniq_categories)
Y = le.transform(categories)

def get_stop_words():
  f = open('data/stopwords.txt')
  L = []
  for w in f:
    L.append(w.strip())
  return set(L)

stops = get_stop_words()

_digits = re.compile('\d')
_weird_chars = re.compile('( |\/|\.|,|;|:|\(|\)|"|\?|!|®|ᴹᴰ|™|\*|\{|\}|\$|_|\\\\|`|\'|\]|\[|<|\+|=|\^|\-|~)')
def contains_digits(d):
  return bool(_digits.search(d))

def tokenize(s):
  s = s.lower()
  tokens = _weird_chars.split(s)
  tokens = filter(lambda x: len(x) > 2 and x not in stops and not contains_digits(x), tokens)
  return tokens

class MultiNaiveBayes:
  def __init__(self):
    self.N = 0
    self.Nc = {}
    self.prior = {}
    self.vocabulary = set()
    self.Tct = {}
    self.condprob = {}
    self.cnb_condprob = {}
    self.sum_Tct = {}
    self.alpha = 1
    self.df = {}
    self.wc = {}

  def fit(self, X, Y):
    self.N = len(Y)
    for x in X:
      words = tokenize(x)
      for word in words:
        self.vocabulary.add(word)
    for c in np.unique(Y):
      indices = np.where(Y == c)
      self.Nc[c] = len(indices) 
      self.prior[c] = self.Nc[c] / self.N
      self.Tct[c] = {}
      self.condprob[c] = {}
      for t in self.vocabulary:
        self.Tct[c][t] = 0
      for i in indices[0].tolist():
        x = X[i]
        self.wc[i] = {}
        words = tokenize(x)
        for word in words:
          if not word in self.wc[i]:
            self.wc[i][word] = 0
          if not word in self.df:
            self.df[word] = 0
          self.df[word] += 1
          self.wc[i][word] += 1

    if SUBLINEAR_TF or USE_IDF or NORMALIZE_DIST:
      for c in self.Nc:
        indices = np.where(Y == c)
        for i in indices[0].tolist():
          for w in self.wc[i]:
            if SUBLINEAR_TF:
              self.wc[i][w] = math.log(self.wc[i][w] + 1) # sub-linear transform
            if USE_IDF:
              self.wc[i][w] *= math.log(self.N / self.df[w]) # IDF transform
          if NORMALIZE_DIST:
            dist = math.sqrt(sum([self.wc[i][w]**2 for w in self.wc[i]]))
            for w in self.wc[i]:
              self.wc[i][w] /= dist
    for c in self.Nc:
      indices = np.where(Y == c)
      for i in indices[0].tolist():
        for t in self.vocabulary:
          if t in self.wc[i]:
            self.Tct[c][t] += self.wc[i][t]

    for c in self.Nc:
      self.sum_Tct[c] = sum([self.Tct[c][t]+self.alpha for t in self.Tct[c]])
      for t in self.vocabulary:
        self.condprob[c][t] = (self.Tct[c][t] + self.alpha) / self.sum_Tct[c]

    for c in self.Nc:
      self.cnb_condprob[c] = {}
      N_c = 0
      for cc in self.sum_Tct:
        if cc != c:
          N_c += self.sum_Tct[cc]

      for t in self.vocabulary:
        N_Tct = 0
        for cc in self.Tct:
          if cc != c:
            N_Tct += self.Tct[cc][t]
        self.cnb_condprob[c][t] = (N_Tct+self.alpha) / N_c

    # normalize weights
    if NORMALIZE_WEIGHTS:
      for c in self.Nc:
        sum_cnb_condprob = sum([self.cnb_condprob[c][t] for t in self.vocabulary])
        sum_condprob = sum([self.condprob[c][t] for t in self.vocabulary])
        for t in self.vocabulary:
          self.cnb_condprob[c][t] /= sum_cnb_condprob
          self.condprob[c][t] /= sum_condprob

  def predict_proba(self, X, category):
    if category == None:
      return [self.predict_proba(X, c) for c in self.Nc]
    score = math.log(self.prior[category])
    for x in X:
      words = tokenize(x)
      for word in words:
        if word in self.vocabulary:
          if USE_COMPLEMENT:
            score -= math.log(self.cnb_condprob[category][word])
          else:
            score += math.log(self.condprob[category][word])
    return score

  def predict(self, X):
    pred = np.zeros((X.shape[0], 1))
    for i, x in enumerate(X):
      ps = self.predict_proba(tokenize(x), None)
      pred[i][0] = np.argmax(ps)
    return pred

X_train, X_test, Y_train, Y_test = train_test_split(titles, Y, test_size=0.2, random_state=42)
clf = MultiNaiveBayes()
print "begin fit"
clf.fit(X_train, Y_train)
print "done fit"
pred = clf.predict(X_test)
score = metrics.f1_score(Y_test, pred)
print metrics.classification_report(Y_test, pred)
print metrics.confusion_matrix(Y_test, pred) 
