# README #
## Categorize abstracts of scientific articles into four categories.
See [here](http://inclass.kaggle.com/c/abstract-classification) for details.

# Setup instructions
This project uses python 2.7

# Install python deps:
```
pip install numpy
pip install sklearn
```

# Run multinomial naive bayes [4]:
```
python bayes.py
```

# Run complement naive bayes [2]:
```
python cnb_bayes.py
```

# kNN (with LSH) [5]
```
python knn.py
```

# Benchmark sparse classifiers [1]. See results [here](https://bitbucket.org/npow/arxiv/wiki/Home)
Classifiers: RidgeClassifier, SGDClassifier, NearestCentroid, LinearSVC (L1 and L2), MultinomialNB, BernoulliNB, Perceptron, PassiveAggressiveClassifier
```
python benchmark.py
```

# Additional deps:
```
pip install contextlib
pip install nltk
pip install snowballstemmer
pip install nolearn
```

# Install nltk deps:
```
# python
> import nltk
> nltk.download()
# download the stopwords package
```

# Neural Network
```
python dbn_test.py
```

# Cython
```
./compile.sh
# python
> import bayes
```

# References
```
[1] http://scikit-learn.org/stable/auto_examples/document_classification_20newsgroups.html
[2] http://people.csail.mit.edu/jrennie/papers/icml03-nb.pdf
[3] http://www.cs.waikato.ac.nz/~eibe/pubs/kibriya_et_al_cr.pdf
[4] http://nlp.stanford.edu/IR-book/pdf/13bayes.pdf
[5] http://infolab.stanford.edu/~ullman/mmds/ch3a.pdf
```